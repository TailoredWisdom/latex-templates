# Rapport UdeS
Ceci est un gabarit pour la mise en forme des rapports et des devoirs rédigés à l'université de Sherbrooke.
Le gabarit contient le minimum nécessaire pour générer la mise en page du document.
Dans le cas où d'autres _packages_ sont nécessaires, il est possible de se référer à la section Examples du répertoire git.

Note : le gabarit est conçu avec le compilateur LuaLaTeX et il est recommandé d'utiliser ce même compilateur.
Il peut être sélectionné dans les paramètres du document OverLeaf ou dans vos outils d'automatisation